const mongoose = require('mongoose');

const NoteSchema = mongoose.Schema({
    title: String,
    content: String
}, {
    timestamps: true,
    collection : "note"
});

module.exports = mongoose.model('Note', NoteSchema);